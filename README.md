# Docker Container for Jenkins


This is conainter for [jenkins][1]. It uses the latest Jenkins weekely release.


## To Build

```
> docker build --tag codered/jenkins . # normal build
> docker build --no-cache=true --force-rm=true --tag codered/jenkins . # force a full build
> docker push codered/jenkins # send it to docker hub
```

## To Run

```
>  docker run --name jenkins \
           --detach \
           --restart unless-stopped \
           --publish 8080:8080 \
           --volume /var/lib/jenkins:/home/jenkins \
           --volume /srv/www:/srv/www \
           codered/jenkins


> docker stop jenkins
> docker start jenkins

```

[1]:  http://jenkins-ci.org/

